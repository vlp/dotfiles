#!/bin/zsh
sudo -H -u vlp zsh -c 'pass maison.vlp.fdn.fr/nuc/hdd_backup' | cryptsetup -v luksOpen /dev/sdc1 partoche
mount -v /dev/mapper/partoche /home/vlp/backup_hdd
rsync -r -t -x -v --progress --del /home/vlp/nextcloud/ /home/vlp/backup_hdd/nextcloud
chown vlp:vlp -R /home/vlp/backup_hdd/nextcloud/
umount -v /home/vlp/backup_hdd
cryptsetup -v luksClose partoche
